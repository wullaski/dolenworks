---
title: Department of Veteran Affairs - Check In Experience
description: A tool used by veterans to check into their appointments.
date: 2024-04-01
tags: ['react', 'ruby', 'drupal', 'cypress', 'mocha', 'chai', 'lambda', 'aws', 'redux']
layout: layouts/project.njk
image: /img/screens/va-check-in-appointments.png
image_alt: A screenshot of the web application.
site_link: https://www.va.gov/
---
This is a web application written in React that serves as a method for Veterans to check in to appointments. It allows for early verification of contact, next of kin, and emergency contact information as well as functionality to submit a travel claim.

I worked on the the front-end code which is entirely React. I've done some work writing code for new AWS lambdas as part of the middleware that the application uses.

<a href="{{ site_link | url }}">Visit va.gov</a>