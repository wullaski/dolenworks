#Docker Desktop Home Server

I'm learning about docker desktop and attempting to set up a home server for hosting things like plex and this website. I don't think it's a great idea given the lack of documeted examples. Most sane people tend to have a dedicated machine for these sorts of things. But I don't and I was curious if I could get it to work on the same machine my son uses for gaming.

I have docker desktop running and the next step is to set up a hello world container with it.

I attempted to install docker desktop on a separate drive just to keep it isolated from my son's gaming computer but it didn't go well.

https://stackoverflow.com/questions/75727062/how-to-install-docker-desktop-on-a-different-drive-location-on-windows

I went back to installing it with the defaults and encoutered the same problem. Read the error message and did what it said and enabled virtualization in the bios settings ran the command it suggested in cmd and it worked.

Now I need to see if I can get to the docker container from the local network. 

Questions:

Do I need another docker container? Can I ssh into the one that is running when I start docker desktop what is that container for? Do other containers get set up in that?

Is it a VM? Is it linux? What version? lots of questions.

Reverse proxy might be a solution for exposing docker on the local network.

https://www.freecodecamp.org/news/docker-nginx-letsencrypt-easy-secure-reverse-proxy-40165ba3aee2/


Docker desktop is a fail need a virtual machine to start with linux install then build on top of that.

https://blog.codyrichardson.io/2020/04/creating-home-lab-with-virtualbox.html

https://joekarlsson.com/2023/09/how-to-get-started-building-a-homelab-server-in-2024/

or virtual box with debian + docker + ddev

maybe I should work backwards?

use git to pull down dockerized projects and figure out networking?

I need to look into tipi once I have a good starting point with virtual box.

